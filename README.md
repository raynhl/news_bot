# Telegram News Bot

This is a sample Telegram chatbot program in Python. It will parse news from The Star Online and send it to the users.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Obtaining API Keys

Firstly, you will need to obtain API key for your Telegram chatbot. 
To do so, carry out the following steps:

```
1) Add @BotFather on your Telegram.
2) Send '/newbot' to BotFather.
3) Enter your bot's name and username.
4) Upon successful creation, BotFather will return a message containing the HTTP API key. Save this key.
```

Secondly, you will need to obtain API key for the Google URL Shortener service. 
Visit the following website to obtain your key:

```
https://developers.google.com/url-shortener/v1/getting_started
```

After you have received these keys, modify the global variables TELEGRAM_CHATBOT_API_KEY & GOOGLE_SHORTERNER_API_KEY to get started.

### Installing

A step by step instruction to get your development running.

You need to download and install the latest Python 2.7 to get started.

After installing Python 2.7, install the libraries in the dependencies folder in the following order.

```
1) urllib3
2) requests
3) feedparser
4) telepot
```

To install a library, open the command prompt from the library's directory and run the following command.

```
python setup.py install
```

### Run the Chatbot

Once you have done all the required installation, open command prompt from the main directory and run the script.

```
python news_bot.py
``` 

## Acknowledgements

* Authors of the libraries listed in the dependencies. They solely own the codes.
* Inspiration
* etc
