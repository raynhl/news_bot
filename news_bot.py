
####### TELEGRAM CHATBOT SETUP ############
########################################### 
# Get Telegram Bot's API Key from BotFather
import telepot
TELEGRAM_CHATBOT_API_KEY = "YOUR-TELEGRAM-API-KEY-HERE"
user_list = []

###########################################
####### TELEGRAM CHATBOT SETUP ############


########### RSS PARSER SETUP ##############
###########################################
import feedparser
import time
from threading import Thread
RSS_URL = "http://www.thestar.com.my/rss/editors-choice/main/"
article_list = []

# Get API KEY @ https://developers.google.com/url-shortener/v1/getting_started
# Uncomment the three lines below if using Google URL Shortener
import json
import requests
GOOGLE_SHORTERNER_API_KEY = "YOUR-GOOGLE-SHORTENER-API-KEY-HERE"

###########################################
########### RSS PARSER SETUP ##############


file_path = "chat_ids.dat"


class MyChatBot:
	def __init__(self):
		self.chatbot = telepot.Bot(TELEGRAM_CHATBOT_API_KEY)
		print("[INFO] Created MyChatbot Object")

	# Callback function when new messages arrive
	def on_chat_message(self, msg):
		global user_list

		content_type, chat_type, chat_id = telepot.glance(msg)

		if (msg["text"] == "/start"):
			print("[NEW USER] Chat_ID: {}".format(chat_id))
			self.chatbot.sendMessage(chat_id, "Welcome to my news chatbot! You will now receive periodic news update.")
			self.save_to_file(chat_id)

			if not chat_id in user_list: 
				user_list.append(chat_id)
			

		else:
			self.chatbot.sendMessage(chat_id, "Please subscribe by sending /start")

	def save_to_file(self, chat_id):
		global user_list

		if not chat_id in user_list:
			file = open(file_path,"a")
			file.write(str(chat_id))
			file.write('\n')
			file.close()
			print("Saved")


	# This is the function to call to broadcast message to all users.
	def send_message_to_all_users(self, message):

		if not user_list:
			print("[ERROR] User_list is empty")
			return

		for chat_id in user_list:
			self.chatbot.sendMessage(chat_id,message)
			time.sleep(0.5)





class Parser:
	def __init__(self, chatbot):
		self.chatbot = chatbot
		print("[INFO] Created Parser Object")

	def get_title(self, feed, index):
		return feed["entries"][index]["title"]

	def get_description(self, feed, index):
		return feed["entries"][index]["description"]

	def get_link(self, feed, index):
		return feed["entries"][index]["link"]

	def get_published_date(self, feed, index):
		return feed["entries"][index]["published"]

	def is_old_article(self,title):
		global article_list
		return (title in article_list)

	# Uncomment this function to use google shortener api
	def google_url_shorten(self, url):
		req_url = 'https://www.googleapis.com/urlshortener/v1/url?key=' + GOOGLE_SHORTERNER_API_KEY
		payload = {'longUrl': url}
		headers = {'content-type': 'application/json'}
		r = requests.post(req_url, data=json.dumps(payload), headers=headers)
		resp = json.loads(r.text)
		return resp['id']

	def refresh(self):
		global article_list

		feed = feedparser.parse(RSS_URL)

		# Check if data fetch is successful. If not, retry two times
		# before returning fail
		count = 1

		while not feed["entries"]:
			if (count > 2):
				print("[ERROR] Feed is dead. Terminating program.")
				return

			print("[ERROR] Unable to get feed. Trying {} time(s).".format(count))
			feed = feedparser.parse(RSS_URL)
			time.sleep(2)
			count += 1

		# If you reach here, it means that you have valid articles from
		# the feed.
		new_article_count = 0

		for i in range(len(feed)):
			title = self.get_title(feed,i)
			if not self.is_old_article(title):
				new_article_count += 1
		
		print("[SUCCESS] {} article(s) found. There are {} new article(s).".format(len(feed), new_article_count))
		print("")

		new_article_count = 1
		# We iterate over all the feed to get individual feed info
		for i in range(len(feed)):
			title = self.get_title(feed,i)
			description = self.get_description(feed,i)
			published_date = self.get_published_date(feed,i)
			
			url = self.get_link(feed,i)
			# Uncomment the line below to use Google URL Shortener
			url = self.google_url_shorten(url)

			
			# Check if it is an old article.
			# Skip if is an old article. 
			# Else, save it in the global article list and publish to Chatbot.
			if not self.is_old_article(title):
				article_list.append(title)
				print("{}) {}".format(new_article_count, title))
				message = title + "\n\n" + description + "\n\n" + published_date + "\n\n"
				message += "Read more @ " 
				message += url

				new_article_count += 1
				# print(message)
				# SEND THE MESSAGE NOW!
				self.chatbot.send_message_to_all_users(message)

		print("")


	def auto_update(self):
		global user_list

		file = open(file_path,"r")
		user_list = file.readlines()
		file.close()
		
		user_list = [int(i) for i in user_list]

		while True:
			if not user_list:
				print("[INFO] There are no users. Sleeping.....")
				time.sleep(120)

			else:
				self.refresh()
				time.sleep(30)


my_chat_bot = MyChatBot()
parser = Parser(my_chat_bot)


parser_thread = Thread(target=parser.auto_update, args=())
parser_thread.start()

my_chat_bot.chatbot.message_loop(my_chat_bot.on_chat_message, run_forever= True)	


